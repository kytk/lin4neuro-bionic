# lin4neuro-bionic
Building script of Lin4Neuro from Scratch

This repository includes scripts to make Lin4Neuro
It also includes installer-scripts for several neuroimaging software.

# How to make Lin4Neuro with your own

## Download scripts

```
git clone https://gitlab.com/kytk/lin4neuro-bionic.git
```

## Run build-l4n-bionic-live-1

```
cd lin4neuro-bionic
./build-l4n-bionic-live-1-en.sh
```

- This script prepares packages to chroot.

## Run build-l4n-bionic-live-2

```
./build-l4n-bionic-live-2-en.sh
```

- This script builds Lin4Neuro within chroot.

## Run build-l4n-bionic-live-3

```
./build-l4n-bionic-live-3-en.sh
```

- This script generates iso image under ~/l4n-build


## Installer for other neuroimaging software packages

I also prepared the installer for popular software packages.

- AFNI
- Aliza
- ANTs
- CONN standalone
- FreeSurfer
- FSL
- ITK-SNAP
- MRtrix3
- SPM12 standalone 

These installer can be found in lin4neuro-bionic/installer.


