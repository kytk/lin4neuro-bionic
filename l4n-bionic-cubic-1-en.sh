#!/bin/bash

# Build Lin4Neuro (Bionic) using Cubic 
# Prerequisite: Ubuntu 18.04 and Cubic

#ChangeLog
# 08-Feb-2021: Refresh script

##### COPY THIS SCRIPT IN THE CUBIC TERMINAL ###################################

rm /etc/apt/sources.list
cat << EOF >> /etc/apt/sources.list
deb http://us.archive.ubuntu.com/ubuntu/ bionic main restricted
deb http://us.archive.ubuntu.com/ubuntu/ bionic-updates main restricted
deb http://us.archive.ubuntu.com/ubuntu/ bionic universe
deb http://us.archive.ubuntu.com/ubuntu/ bionic-updates universe
deb http://us.archive.ubuntu.com/ubuntu/ bionic multiverse
deb http://us.archive.ubuntu.com/ubuntu/ bionic-updates multiverse
deb http://us.archive.ubuntu.com/ubuntu/ bionic-backports main restricted universe multiverse
deb http://security.ubuntu.com/ubuntu bionic-security main restricted
deb http://security.ubuntu.com/ubuntu bionic-security universe
deb http://security.ubuntu.com/ubuntu bionic-security multiverse
EOF

apt-get update
apt-get dist-upgrade -y
apt-get install -y gnupg wget

# Neurodebian (us-nh)
wget -O- http://neuro.debian.net/lists/bionic.us-nh.full | \
tee /etc/apt/sources.list.d/neurodebian.sources.list
apt-key adv --recv-keys --keyserver hkps://keyserver.ubuntu.com 0xA5D32F012649A5A9
#gpg --keyserver hkp://pool.sks-keyservers.net:80 --recv-keys 0xA5D32F012649A5A9
#gpg -a --export 0xA5D32F012649A5A9 | apt-key add -

# XFCE
apt-get -y install xfce4 xfce4-terminal xfce4-indicator-plugin \
 xfce4-power-manager-plugins lightdm lightdm-gtk-greeter \
 shimmer-themes network-manager-gnome xinit build-essential \
 dkms thunar-archive-plugin file-roller gawk fonts-noto \
 xdg-utils 

# Python-related packages
apt-get -y install pkg-config libopenblas-dev liblapack-dev \
 libhdf5-serial-dev graphviz python3-venv python3-pip \
 python3-dev python3-tk jupyter-notebook python-numpy
pip3 install bash_kernel
python3 -m bash_kernel.install

# Utilities
apt-get -y install at-spi2-core bc byobu curl dc \
 default-jre evince exfat-fuse exfat-utils gedit \
 gnome-system-monitor gnome-system-tools gparted \
 imagemagick rename ntp system-config-printer-gnome \
 system-config-samba tree unzip update-manager vim \
 wajig xfce4-screenshooter zip ntp tcsh baobab xterm \
 bleachbit libopenblas-base cups apturl dmz-cursor-theme \
 chntpw gddrescue p7zip-full git

# Workaround for system-config-samba and networking devices
touch /etc/libuser.conf
touch /etc/NetworkManager/conf.d/10-globally-managed-devices.conf

# Vim settings
cp /usr/share/vim/vimrc /etc/skel/.vimrc
sed -i -e 's/"set background=dark/set background=dark/' /etc/skel/.vimrc

# Firefox and Libreoffice
apt-get -y install firefox firefox-locale-en libreoffice libreoffice-help-en-us

# Remove xscreensaver
apt-get -y purge xscreensaver

# Lin4Neuro
git clone https://gitlab.com/kytk/lin4neuro-bionic.git
cd lin4neuro-bionic

# Working directory
base_path=$PWD/lin4neuro-parts

# Plymouth
apt-get -y install plymouth-themes plymouth

# Lin4neuro-logo
cp -r "${base_path}"/lin4neuro-logo /usr/share/plymouth/themes
update-alternatives --install \
 /usr/share/plymouth/themes/default.plymouth \
 default.plymouth \
 /usr/share/plymouth/themes/lin4neuro-logo/lin4neuro-logo.plymouth \
 100
update-alternatives --config default.plymouth
update-initramfs -u 

# Icons
mkdir -p /etc/skel/.local/share
cp -r "${base_path}"/local/share/icons /etc/skel/.local/share/

# Customized menu
mkdir -p /etc/skel/.config/menus
cp "${base_path}"/config/menus/xfce-applications.menu \
	/etc/skel/.config/menus

# Desktop files
cp -r "${base_path}"/local/share/applications /etc/skel/.local/share/

# Neuroimaging.directory
mkdir -p /etc/skel/.local/share/desktop-directories
cp "${base_path}"/local/share/desktop-directories/Neuroimaging.directory /etc/skel/.local/share/desktop-directories

# Background image
cp "${base_path}"/backgrounds/deep_ocean.png /usr/share/backgrounds
rm /usr/share/backgrounds/xfce/xfce-teal.jpg

# Modified lightdm-gtk-greeter.conf
mkdir -p /usr/share/lightdm/lightdm-gtk-greeter.conf.d
cp "${base_path}"/lightdm/lightdm-gtk-greeter.conf.d/01_ubuntu.conf /usr/share/lightdm/lightdm-gtk-greeter.conf.d

# Auto-login
mkdir -p /usr/share/lightdm/lightdm.conf.d
cp "${base_path}"/lightdm/lightdm.conf.d/10-ubuntu.conf \
	/usr/share/lightdm/lightdm.conf.d

# Cusotmized panel, dsktop, and theme
cp -r "${base_path}"/config/xfce4 /etc/skel/.config

# Copy lin4neuro-bionic to /etc/skel
mkdir /etc/skel/git
cp -r ../lin4neuro-bionic /etc/skel/git/

# Delete ubuntu-desktop
apt-get purge -y nautilus gnome-power-manager gnome-screensaver gnome-termina* \
  gnome-pane* gnome-applet* gnome-bluetooth gnome-desktop* gnome-sessio* \
  gnome-user* gnome-shell-common compiz compiz* unity unity* hud zeitgeist \
  zeitgeist* python-zeitgeist libzeitgeist* activity-log-manager-common \
  gnome-control-center gnome-screenshot overlay-scrollba* 
apt-get install -y gnome-software eog
 
# Clean packages
apt-get -y autoremove

# Remove unnecessary configuration files
dpkg -l | awk '/^rc/ {print $2}' | xargs dpkg --purge

# Alias
cat << EOS >> /etc/skel/.bashrc

#alias for xdg-open
alias open='xdg-open &> /dev/null'

EOS

# Post-installation script
cat << 'EOS' >> /etc/skel/.profile

#Post installation
if [ $(id -u) -ne 999 ]; then 
  if [ ! -d /usr/local/MRIcroGL ]; then
      cd ~/git/lin4neuro-bionic
      xfce4-terminal -x './l4n-bionic-cubic-2.sh' &
  fi
fi

cd $HOME 

EOS

echo "Done."

exit

