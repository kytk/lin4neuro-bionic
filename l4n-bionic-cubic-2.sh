#!/bin/bash

# Build Lin4Neuro (Bionic) using Cubic: Part 2

# ChangeLog
# 08-Feb-2021: Refresh script

##### STEP 0. PREPARATION ######################################################
# Log
log=$(date +%Y%m%d%H%M%S)-vm2.log
exec &> >(tee -a "$log")


# Settings for Japanese
if [ $LANG == "ja_JP.UTF-8" ]; then
  LANG=C xdg-user-dirs-update --force
  cd $HOME
  if [ -d ダウンロード ]; then
    rm -rf ダウンロード テンプレート デスクトップ ドキュメント ビデオ \
           ピクチャ ミュージック 公開
  fi
  im-config -n fcitx
fi


##### STEP 1. INSTALL NEUROIMAGING SOFTWARE PACKAGES USING APT #################

echo "Install neuroimaging-related software packages"
echo "Please enter password when prompted"

#DCMTK
echo "Install DCMTK"
sudo apt-get install -y dcmtk


#Octave
sudo apt-get install -y octave
sudo pip3 install octave_kernel


#R
sudo apt-get install -y r-base


#R (cloud.r-project.org)
#sudo apt-key adv --keyserver keyserver.ubuntu.com \
#     --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
#
#echo "Install R using cran.rstudio.com repository"
#
#grep rstudio /etc/apt/sources.list > /dev/null
#if [ $? -eq 1 ]; then
#  sudo add-apt-repository \
#  'deb https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/'
#fi
#sudo apt-get -y update


##### STEP 2. INSTALL NEUROIMAGING SOFTWARE PACKAGES MANUALLY  #################

touch ~/.bash_aliases

# libjpeg62
libjpeg62_indicator=$(dpkg -l | grep libjpeg62 | cut -c 1-2)
if [ "$libjpeg62_indicator" != "ii" ]; then
  echo "install libjpeg62"
  cd $HOME/Downloads
  curl -O http://security.ubuntu.com/ubuntu/pool/universe/libj/libjpeg6b/libjpeg62_6b2-3_amd64.deb
  sudo apt install -y ./libjpeg62_6b2-3_amd64.deb
fi


# libpng12
libpng12_indicator=$(dpkg -l | grep libpng12 | cut -c 1-2)
if [ "$libpng12_indicator" != "ii" ]; then
  echo "install libpng12"
  cd $HOME/Downloads
  curl -O http://security.ubuntu.com/ubuntu/pool/main/libp/libpng/libpng12-0_1.2.54-1ubuntu1.1_amd64.deb
  sudo apt install -y ./libpng12-0_1.2.54-1ubuntu1.1_amd64.deb
fi


#Mango
echo "Install Mango"
cd $HOME/Downloads

if [ ! -e 'mango_unix.zip' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/mango_unix.zip
fi

cd /usr/local
sudo unzip ~/Downloads/mango_unix.zip

grep Mango ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> ~/.bash_aliases
    echo '# Mango' >> ~/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/Mango' >> ~/.bash_aliases
fi


#MRIcroGL
echo "Install MRIcroGL"
cd $HOME/Downloads

curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/MRIcroGL_linux.zip

cd /usr/local
sudo unzip ~/Downloads/MRIcroGL_linux.zip

grep MRIcroGL ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    sudo echo '' >> ~/.bash_aliases
    sudo echo '# MRIcroGL' >> ~/.bash_aliases
    sudo echo 'export PATH=$PATH:/usr/local/MRIcroGL' >> ~/.bash_aliases
    sudo echo 'export PATH=$PATH:/usr/local/MRIcroGL/Resources' >> ~/.bash_aliases
fi


# MRIcron
cd $HOME/Downloads

if [ ! -e 'MRIcron_linux.zip' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/MRIcron_linux.zip
fi

cd /usr/local
sudo unzip ~/Downloads/MRIcron_linux.zip

sudo find /usr/local/mricron -name 'dcm2niix' -exec rm {} \;
 # Delete dcm2niix because this version is old

sudo find /usr/local/mricron -name '*.bat' -exec rm {} \;
 # Delete batch files since they are not available anymore

sudo find /usr/local/mricron -type d -exec chmod 755 {} \;
sudo find /usr/local/mricron/Resources -type f -exec chmod 644 {} \;
sudo chmod 755 /usr/local/mricron/Resources/pigz_mricron

grep mricron ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    sudo echo '' >> ~/.bash_aliases
    sudo echo '# MRIcron' >> ~/.bash_aliases
    sudo echo 'export PATH=$PATH:/usr/local/mricron' >> ~/.bash_aliases
fi


# ROBEX
echo "Install ROBEX"
cd $HOME/Downloads

if [ ! -e 'ROBEXv12.linux64.tar.gz' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/ROBEXv12.linux64.tar.gz
fi

cd /usr/local
sudo tar xvzf ~/Downloads/ROBEXv12.linux64.tar.gz
sudo chmod 755 ROBEX
cd ROBEX
sudo find -type f -exec chmod 644 {} \;
sudo chmod 755 ROBEX runROBEX.sh dat ref_vols

grep ROBEX ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> ~/.bash_aliases
    echo '# ROBEX' >> ~/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/ROBEX' >> ~/.bash_aliases
fi


# Surf-Ice
cd $HOME/Downloads
curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/surfice_linux.zip

cd /usr/local
sudo unzip ~/Downloads/surfice_linux.zip

cd Surf_Ice
sudo find . -type d -exec chmod 755 {} \;
sudo find . -type f -exec chmod 644 {} \;
sudo chmod 755 surfice*
sudo chmod 644 surfice_Linux_Installation.txt

grep Surf_Ice ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    sudo echo '' >> ~/.bash_aliases
    sudo echo '# Surf_Ice' >> ~/.bash_aliases
    sudo echo 'export PATH=$PATH:/usr/local/Surf_Ice' >> ~/.bash_aliases
fi


# Talairach daemon
echo "Install Talairach daemon"
sudo cp -r ~/git/lin4neuro-bionic/lin4neuro-parts/tdaemon /usr/local
grep Talairach ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
  echo '' >> ~/.bash_aliases
  echo '# Talairach daemon' >> ~/.bash_aliases
  echo "alias tdaemon='java -jar /usr/local/tdaemon/talairach.jar'" >> ~/.bash_aliases
fi


# VirtualMRI
echo "Install Virtual MRI"
cd $HOME/Downloads

if [ ! -e 'vmri.zip' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/vmri.zip
fi

cd /usr/local
sudo unzip ~/Downloads/vmri.zip


# Tutorial
echo "Install tutorial by Chris Rorden"
cd $HOME/Downloads

if [ ! -e 'tutorial.zip' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/tutorial.zip
fi

cd $HOME
unzip ~/Downloads/tutorial.zip
find tutorial -type f -exec chmod 644 {} \;
find tutorial -type d -exec chmod 755 {} \;


##### STEP 3. POST-INSTALLATION TUNING #########################################

# Remove unnecessary files
sudo find / -name '__MACOSX' -exec rm -rf {} \;
sudo find / -name '.DS_Store' -exec rm -rf {} \;
sudo find / -name '._*' -exec rm {} \;
sudo find / -name 'Thumbs.db' -exec rm {} \;


# Symbolic link to the installer
sudo cat << EOS >> ~/.profile

# symbolic link to the installer
if [ ! -L ~/Desktop/installer ]; then
   ln -fs ~/git/lin4neuro-bionic/installer ~/Desktop
fi

EOS

# Change /bin/sh from dash to bash
echo "dash dash/sh boolean false" | sudo debconf-set-selections
sudo dpkg-reconfigure --frontend=noninteractive dash

echo "Finished!"

exit
