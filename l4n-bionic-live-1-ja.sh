#!/bin/bash
# build L4N from scratch: Japanese
# Part 1: Prepare chroot environment
# Part of this script is based on "live-custom-ubuntu-from-scratch"
# https://github.com/mvallim/live-custom-ubuntu-from-scratch

cat << EOS
##### ATTENTION ########################################################################################

This script is in its alpha state.
Please do not use this script yet.

########################################################################################################
EOS


# set -x

LANG=C
sudo apt-get update; sudo apt-get -y upgrade

echo "Begin making Lin4Neuro live Japanese version from scratch."

# Set variables
ts=$(date +%Y%m%d)
wd=l4n-build/l4n-${ts}-ja

# Install applications to build the environment
sudo apt-get update && sudo apt-get -y upgrade
sudo apt-get install -y \
    binutils debootstrap squashfs-tools xorriso \
    grub-pc-bin grub-efi-amd64-bin mtools

# Prepare working directory
mkdir -p $HOME/$wd
 
# Debootstrap
sudo debootstrap \
    --arch=amd64 \
    --variant=minbase \
    bionic \
    $HOME/$wd/chroot \
    http://jp.archive.ubuntu.com/ubuntu/

# Configure external mount points
sudo mount --bind /dev $HOME/$wd/chroot/dev
sudo mount --bind /run $HOME/$wd/chroot/run

# Copy l4n-bionic-live-2-ja.sh to chroot
sudo cp ./l4n-bionic-live-2-ja.sh $HOME/$wd/chroot
sudo chmod 755 $HOME/$wd/chroot/l4n-bionic-live-2-ja.sh
echo "Please run l4n-bionic-live-2-ja.sh"
sudo chroot $HOME/$wd/chroot

