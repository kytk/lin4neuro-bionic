#!/bin/bash

# Build Lin4Neuro (Bionic) using Cubic 
# Prerequisite: Ubuntu 18.04 and Cubic

#ChangeLog
# 08-Feb-2021: Refresh script

rm /etc/apt/sources.list
cat << EOF >> /etc/apt/sources.list
deb http://us.archive.ubuntu.com/ubuntu/ bionic main restricted
deb http://us.archive.ubuntu.com/ubuntu/ bionic-updates main restricted
deb http://us.archive.ubuntu.com/ubuntu/ bionic universe
deb http://us.archive.ubuntu.com/ubuntu/ bionic-updates universe
deb http://us.archive.ubuntu.com/ubuntu/ bionic multiverse
deb http://us.archive.ubuntu.com/ubuntu/ bionic-updates multiverse
deb http://us.archive.ubuntu.com/ubuntu/ bionic-backports main restricted universe multiverse
deb http://security.ubuntu.com/ubuntu bionic-security main restricted
deb http://security.ubuntu.com/ubuntu bionic-security universe
deb http://security.ubuntu.com/ubuntu bionic-security multiverse
EOF

apt-get update
apt-get dist-upgrade -y
apt-get install -y gnupg wget

# Neurodebian (us-nh)
wget -O- http://neuro.debian.net/lists/bionic.us-nh.full | \
tee /etc/apt/sources.list.d/neurodebian.sources.list
apt-key adv --recv-keys --keyserver hkps://keyserver.ubuntu.com 0xA5D32F012649A5A9
#gpg --keyserver hkp://pool.sks-keyservers.net:80 --recv-keys 0xA5D32F012649A5A9
#gpg -a --export 0xA5D32F012649A5A9 | apt-key add -

# XFCE
apt-get -y install xfce4 xfce4-terminal xfce4-indicator-plugin \
 xfce4-power-manager-plugins lightdm lightdm-gtk-greeter \
 shimmer-themes network-manager-gnome xinit build-essential \
 dkms thunar-archive-plugin file-roller gawk fonts-noto \
 xdg-utils 

# Python-related packages
apt-get -y install pkg-config libopenblas-dev liblapack-dev \
 libhdf5-serial-dev graphviz python3-venv python3-pip \
 python3-dev python3-tk jupyter-notebook python-numpy
pip3 install bash_kernel
python3 -m bash_kernel.install

# Utilities
apt-get -y install at-spi2-core bc byobu curl dc \
 default-jre evince exfat-fuse exfat-utils gedit \
 gnome-system-monitor gnome-system-tools gparted \
 imagemagick rename ntp system-config-printer-gnome \
 system-config-samba tree unzip update-manager vim \
 wajig xfce4-screenshooter zip ntp tcsh baobab xterm \
 bleachbit libopenblas-base cups apturl dmz-cursor-theme \
 chntpw gddrescue p7zip-full git

# Workaround for system-config-samba and networking devices
touch /etc/libuser.conf
touch /etc/NetworkManager/conf.d/10-globally-managed-devices.conf

# Vim settings
cp /usr/share/vim/vimrc /etc/skel/.vimrc
sed -i -e 's/"set background=dark/set background=dark/' /etc/skel/.vimrc

# Firefox and Libreoffice
apt-get -y install firefox firefox-locale-en libreoffice libreoffice-help-en-us

# Remove xscreensaver
apt-get -y purge xscreensaver

# Lin4Neuro
git clone https://gitlab.com/kytk/lin4neuro-bionic.git
cd lin4neuro-bionic

# Working directory
base_path=$PWD/lin4neuro-parts

# Plymouth
apt-get -y install plymouth-themes plymouth

# Lin4neuro-logo
cp -r "${base_path}"/lin4neuro-logo /usr/share/plymouth/themes
update-alternatives --install \
 /usr/share/plymouth/themes/default.plymouth \
 default.plymouth \
 /usr/share/plymouth/themes/lin4neuro-logo/lin4neuro-logo.plymouth \
 100
update-alternatives --config default.plymouth
update-initramfs -u 

# Icons
mkdir -p /etc/skel/.local/share
cp -r "${base_path}"/local/share/icons /etc/skel/.local/share/

# Customized menu
mkdir -p /etc/skel/.config/menus
cp "${base_path}"/config/menus/xfce-applications.menu \
	/etc/skel/.config/menus

# Desktop files
cp -r "${base_path}"/local/share/applications /etc/skel/.local/share/

# Neuroimaging.directory
mkdir -p /etc/skel/.local/share/desktop-directories
cp "${base_path}"/local/share/desktop-directories/Neuroimaging.directory /etc/skel/.local/share/desktop-directories

# Background image
cp "${base_path}"/backgrounds/deep_ocean.png /usr/share/backgrounds
rm /usr/share/backgrounds/xfce/xfce-teal.jpg

# Modified lightdm-gtk-greeter.conf
mkdir -p /usr/share/lightdm/lightdm-gtk-greeter.conf.d
cp "${base_path}"/lightdm/lightdm-gtk-greeter.conf.d/01_ubuntu.conf /usr/share/lightdm/lightdm-gtk-greeter.conf.d

# Auto-login
mkdir -p /usr/share/lightdm/lightdm.conf.d
cp "${base_path}"/lightdm/lightdm.conf.d/10-ubuntu.conf \
	/usr/share/lightdm/lightdm.conf.d

# Cusotmized panel, dsktop, and theme
cp -r "${base_path}"/config/xfce4 /etc/skel/.config

# Copy lin4neuro-bionic to /etc/skel
mkdir /etc/skel/git
cp -r ../lin4neuro-bionic /etc/skel/git/

# Delete ubuntu-desktop
apt-get purge -y nautilus gnome-power-manager gnome-screensaver gnome-termina* \
  gnome-pane* gnome-applet* gnome-bluetooth gnome-desktop* gnome-sessio* \
  gnome-user* gnome-shell-common compiz compiz* unity unity* hud zeitgeist \
  zeitgeist* python-zeitgeist libzeitgeist* activity-log-manager-common \
  gnome-control-center gnome-screenshot overlay-scrollba* 
apt-get install -y gnome-software eog
 
# Clean packages
apt-get -y autoremove

# Remove unnecessary configuration files
dpkg -l | awk '/^rc/ {print $2}' | xargs dpkg --purge

# Alias
cat << EOS >> /etc/skel/.bash_aliases

#alias for xdg-open
alias open='xdg-open &> /dev/null'

EOS


# Neuroimaging tools

#DCMTK
apt-get install -y dcmtk

#Octave
apt-get install -y octave
pip3 install octave_kernel


#R
apt-get install -y r-base


#Mango
cd
curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/mango_unix.zip

unzip mango_unix.zip -d /usr/local

grep Mango /etc/skel/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> /etc/skel/.bash_aliases
    echo '# Mango' >> /etc/skel/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/Mango' >> /etc/skel/.bash_aliases
fi


#MRIcroGL
cd
curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/MRIcroGL_linux.zip

unzip MRIcroGL_linux.zip -d /usr/local

grep MRIcroGL /etc/skel/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    sudo echo '' >> /etc/skel/.bash_aliases
    sudo echo '# MRIcroGL' >> /etc/skel/.bash_aliases
    sudo echo 'export PATH=$PATH:/usr/local/MRIcroGL' >> /etc/skel/.bash_aliases
    sudo echo 'export PATH=$PATH:/usr/local/MRIcroGL/Resources' >> /etc/skel/.bash_aliases
fi


# MRIcron
cd
curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/MRIcron_linux.zip

unzip MRIcron_linux.zip -d /usr/local

find /usr/local/mricron -name 'dcm2niix' -exec rm {} \;
 # Delete dcm2niix because this version is old

find /usr/local/mricron -name '*.bat' -exec rm {} \;
 # Delete batch files since they are not available anymore

find /usr/local/mricron -type d -exec chmod 755 {} \;
find /usr/local/mricron/Resources -type f -exec chmod 644 {} \;
chmod 755 /usr/local/mricron/Resources/pigz_mricron

grep mricron /etc/skel/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    sudo echo '' >> /etc/skel/.bash_aliases
    sudo echo '# MRIcron' >> /etc/skel/.bash_aliases
    sudo echo 'export PATH=$PATH:/usr/local/mricron' >> /etc/skel/.bash_aliases
fi


# ROBEX
cd
curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/ROBEXv12.linux64.tar.gz

tar xvzf ROBEXv12.linux64.tar.gz -C /usr/local

chmod 755 /usr/local/ROBEX
cd /usr/local/ROBEX
find -type f -exec chmod 644 {} \;
chmod 755 ROBEX runROBEX.sh dat ref_vols

grep ROBEX /etc/skel/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> /etc/skel/.bash_aliases
    echo '# ROBEX' >> /etc/skel/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/ROBEX' >> /etc/skel/.bash_aliases
fi


# Surf-Ice
cd
curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/surfice_linux.zip

unzip surfice_linux.zip -d /usr/local

cd /usr/local/Surf_Ice
sudo find . -type d -exec chmod 755 {} \;
sudo find . -type f -exec chmod 644 {} \;
sudo chmod 755 surfice*
sudo chmod 644 surfice_Linux_Installation.txt

grep Surf_Ice /etc/skel/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    sudo echo '' >> /etc/skel/.bash_aliases
    sudo echo '# Surf_Ice' >> /etc/skel/.bash_aliases
    sudo echo 'export PATH=$PATH:/usr/local/Surf_Ice' >> /etc/skel/.bash_aliases
fi

# Talairach daemon
cd
cp -r lin4neuro-bionic/lin4neuro-parts/tdaemon /usr/local
grep Talairach /etc/skel/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
  echo '' >> /etc/skel/.bash_aliases
  echo '# Talairach daemon' >> /etc/skel/.bash_aliases
  echo "alias tdaemon='java -jar /usr/local/tdaemon/talairach.jar'" >> /etc/skel/.bash_aliases
fi


# VirtualMRI
cd
curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/vmri.zip

unzip vmri.zip -d /usr/local


# Tutorial
cd
curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/tutorial.zip

unzip tutorial.zip -d /etc/skel/
find /etc/skel/tutorial -type f -exec chmod 644 {} \;
find /etc/skel/tutorial -type d -exec chmod 755 {} \;


# Remove unnecessary files
find / -name '__MACOSX' -exec rm -rf {} \;
find / -name '.DS_Store' -exec rm -rf {} \;
find / -name '._*' -exec rm {} \;
find / -name 'Thumbs.db' -exec rm {} \;


# Symbolic link to the installer
cat << EOS >> /etc/skel/.profile

# symbolic link to the installer
if [ ! -L ~/Desktop/installer ]; then
   ln -fs ~/git/lin4neuro-bionic/installer ~/Desktop
fi

EOS

# Change /bin/sh from dash to bash
echo "dash dash/sh boolean false" | debconf-set-selections
dpkg-reconfigure --frontend=noninteractive dash

exit
