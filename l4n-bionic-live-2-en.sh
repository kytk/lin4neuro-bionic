#!/bin/bash
# build L4N from scratch: English
# Part 2: Operation inside chroot
# Some part of this script is based on "live-custom-ubuntu-from-scratch"
#  https://github.com/mvallim/live-custom-ubuntu-from-scratch


cat << EOS
##### ATTENTION ########################################################################################

This script is in its alpha state.
Please do not use this script yet.

########################################################################################################
EOS


##### PREPARE BASE SYSTEM WITH XFCE DESKTOP ENVIRONMENT #####

# Configure mount points, home and locale
mount none -t proc /proc
mount none -t sysfs /sys
mount none -t devpts /dev/pts
export HOME=/root
export LC_ALL=C


# Log
log=$(date +%Y%m%d%H%M%S)-chroot.log
exec > >(tee -a ./${log}) 2>&1


# Set a custom hostname
echo "l4n-live" > /etc/hostname


# Configure apt sources.list
cat <<EOF > /etc/apt/sources.list
deb http://us.archive.ubuntu.com/ubuntu/ bionic main restricted universe multiverse
deb-src http://us.archive.ubuntu.com/ubuntu/ bionic main restricted universe multiverse

deb http://us.archive.ubuntu.com/ubuntu/ bionic-security main restricted universe multiverse
deb-src http://us.archive.ubuntu.com/ubuntu/ bionic-security main restricted universe multiverse

deb http://us.archive.ubuntu.com/ubuntu/ bionic-updates main restricted universe multiverse
deb-src http://us.archive.ubuntu.com/ubuntu/ bionic-updates main restricted universe multiverse
EOF


# Install systemd
apt-get update
apt-get upgrade -y
apt-get install -y libterm-readline-gnu-perl systemd systemd-sysv


# Configure machine-id and divert
dbus-uuidgen > /etc/machine-id
ln -fs /etc/machine-id /var/lib/dbus/machine-id

dpkg-divert --local --rename --add /sbin/initctl
ln -s /bin/true /sbin/initctl


# Install packages needed for Live System
apt-get install -y sudo ubuntu-standard casper lupin-casper discover laptop-detect os-prober \
 network-manager resolvconf net-tools wireless-tools wpagui locales \
 grub-common grub-gfxpayload-lists grub-pc grub-pc-bin grub2-common

# Install linux-generic
apt-get install -y --no-install-recommends linux-generic

# Install graphic installer
apt-get install -y ubiquity ubiquity-casper ubiquity-frontend-gtk ubuntu-drivers-common


# Install XFCE
apt-get install -y xfce4 xfce4-terminal xfce4-indicator-plugin xfce4-power-manager-plugins \
 lightdm lightdm-gtk-greeter shimmer-themes network-manager-gnome xinit build-essential \
 dkms thunar-archive-plugin file-roller gawk xdg-utils 


# Install python-related packages
# python2-numpy is needed for FSL aff2reg
apt-get install -y pkg-config libopenblas-dev liblapack-dev libhdf5-serial-dev graphviz \
 python3-venv python3-pip python3-dev python3-tk jupyter-notebook python-numpy
pip3 install bash_kernel
python3 -m bash_kernel.install


# Install misc packages
apt-get install -y apturl at-spi2-core baobab bc bleachbit chntpw cups curl dc default-jre \
 dmz-cursor-theme eog evince exfat-fuse exfat-utils gddrescue gdebi gedit git \
 gnome-system-monitor gnome-system-tools gnupg gparted imagemagick libopenblas-base meld ntp \
 p7zip-full rename system-config-printer-gnome system-config-samba tcsh tree unzip update-manager \
 vim wget xfce4-screenshooter xterm zip


# Install English-related packages
apt-get install -y firefox firefox-locale-en libreoffice libreoffice-help-en-us language-pack-en tzdata


# Workaround for system-config-samba
touch /etc/libuser.conf


# Workaround for networking devices
touch /etc/NetworkManager/conf.d/10-globally-managed-devices.conf


# Vim settings
cp /usr/share/vim/vimrc /etc/skel/.vimrc
sed -i -e 's/"set background=dark/set background=dark/' /etc/skel/.vimrc


#Remove xscreensaver
apt-get purge -y xscreensaver


##### LIN4NEURO #####

# Lin4Neuro repository
mkdir /etc/skel/git && cd /etc/skel/git
git clone https://gitlab.com/kytk/lin4neuro-bionic.git


# Neurodebian
wget -O- http://neuro.debian.net/lists/bionic.us-nh.full | \
 tee /etc/apt/sources.list.d/neurodebian.sources.list
apt-key add /etc/skel/git/lin4neuro-bionic/neuro.debian.net.asc


# Lin4Neuro Logo
apt-get install -y plymouth-themes plymouth-label
parts=/etc/skel/git/lin4neuro-bionic/lin4neuro-parts
cp -r ${parts}/lin4neuro-logo /usr/share/plymouth/themes
update-alternatives --install /usr/share/plymouth/themes/default.plymouth default.plymouth \
 /usr/share/plymouth/themes/lin4neuro-logo/lin4neuro-logo.plymouth 100
update-initramfs -u -k all


# Icons
mkdir -p /etc/skel/.local/share
cp -r ${parts}/local/share/icons /etc/skel/.local/share/


# Customized menu
mkdir -p /etc/skel/.config/menus
cp ${parts}/config/menus/xfce-applications.menu /etc/skel/.config/menus


# .desktop files
cp -r ${parts}/local/share/applications /etc/skel/.local/share/


# Neuroimaging.directory
mkdir -p /etc/skel/.local/share/desktop-directories
cp ${parts}/local/share/desktop-directories/Neuroimaging.directory \
   /etc/skel/.local/share/desktop-directories


# Background image and remove an unnecessary image file
cp ${parts}/backgrounds/deep_ocean.png /usr/share/backgrounds
rm /usr/share/backgrounds/xfce/xfce-teal.jpg


# Modified lightdm-gtk-greeter.conf
mkdir -p /usr/share/lightdm/lightdm-gtk-greeter.conf.d
cp ${parts}/lightdm/lightdm-gtk-greeter.conf.d/01_ubuntu.conf \
   /usr/share/lightdm/lightdm-gtk-greeter.conf.d


# Auto-login settings
mkdir -p /usr/share/lightdm/lightdm.conf.d
cp ${parts}/lightdm/lightdm.conf.d/10-ubuntu.conf /usr/share/lightdm/lightdm.conf.d


# Customized panel, desktop, and theme
cp -r ${parts}/config/xfce4 /etc/skel/.config


# Clean packages
apt-get autoremove -y


# GRUB customization to show GRUB on boot
sed -i -e 's/GRUB_TIMEOUT_STYLE/#GRUB_TIMEOUT_STYLE/' /etc/default/grub
sed -i -e 's/GRUB_TIMEOUT=0/GRUB_TIMEOUT=10/' /etc/default/grub
update-grub


# Boot repair
add-apt-repository -y ppa:yannubuntu/boot-repair
apt-get install -y boot-repair


# alias
cat << EOS >> /etc/skel/.bash_aliases
#alias for xdg-open
alias open='xdg-open &> /dev/null'

EOS


# Reconfigure packages
dpkg-reconfigure locales
dpkg-reconfigure resolvconf


# Configure network-manager
cat <<EOF > /etc/NetworkManager/NetworkManager.conf
[main]
rc-manager=resolvconf
plugins=ifupdown,keyfile
dns=dnsmasq

[ifupdown]
managed=false
EOF


# Reconfigure network-manager
dpkg-reconfigure network-manager


##### SOFTWARE PACKAGES PREINSTALLED IN LIN4NEURO #####

# Octave
apt-get install -y octave
pip3 install octave_kernel

## R (cloud.r-project.org)
#apt-key adv --keyserver keyserver.ubuntu.com \
#     --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
#grep rstudio /etc/apt/sources.list > /dev/null
#if [ $? -eq 1 ]; then
#  add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/'
#fi
#apt-get -y update


# R
apt-get install -y r-base


# DCMTK
apt-get install -y dcmtk


# Talairach daemon
cp -r ${parts}/tdaemon /usr/local
grep tdaemon /etc/skel/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
  echo '' >> /etc/skel/.bash_aliases
  echo '#tdaemon' >> /etc/skel/.bash_aliases
  echo "alias tdaemon='java -jar /usr/local/tdaemon/talairach.jar'" >> /etc/skel/.bash_aliases
fi


# VirtualMRI
[ ! -d $HOME/Downloads ] && mkdir $HOME/Downloads
cd $HOME/Downloads
if [ ! -e 'vmri.zip' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/vmri.zip
fi
cd /usr/local
unzip ~/Downloads/vmri.zip


# ROBEX
cd $HOME/Downloads
if [ ! -e 'ROBEXv12.linux64.tar.gz' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/ROBEXv12.linux64.tar.gz
fi
cd /usr/local
tar xvzf ~/Downloads/ROBEXv12.linux64.tar.gz
chmod 755 ROBEX
cd ROBEX
find -type f -exec chmod 644 {} \;
chmod 755 ROBEX runROBEX.sh dat ref_vols
grep ROBEX /etc/skel/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> /etc/skel/.bash_aliases
    echo '#ROBEX' >> /etc/skel/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/ROBEX' >> /etc/skel/.bash_aliases
fi


# libpng12
cd $HOME/Downloads
curl -O http://security.ubuntu.com/ubuntu/pool/main/libp/libpng/libpng12-0_1.2.54-1ubuntu1.1_amd64.deb
dpkg -i libpng12-0_1.2.54-1ubuntu1.1_amd64.deb


# Mango
cd "$HOME"/Downloads
if [ ! -e 'mango_unix.zip' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/mango_unix.zip
fi
cd /usr/local
unzip ~/Downloads/mango_unix.zip
grep Mango /etc/skel/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> /etc/skel/.bash_aliases
    echo '#Mango' >> /etc/skel/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/Mango' >> /etc/skel/.bash_aliases
fi


# MRIcroGL
cd "$HOME"/Downloads
curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/MRIcroGL_linux.zip
cd /usr/local
unzip ~/Downloads/MRIcroGL_linux.zip
grep MRIcroGL /etc/skel/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> /etc/skel/.bash_aliases
    echo '#MRIcroGL' >> /etc/skel/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/MRIcroGL' >> /etc/skel/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/MRIcroGL/Resources' >> /etc/skel/.bash_aliases
fi


# MRIcron
cd "$HOME"/Downloads
if [ ! -e 'MRIcron_linux.zip' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/MRIcron_linux.zip
fi
cd /usr/local
unzip ~/Downloads/MRIcron_linux.zip
# Delete dcm2niix because this version is old
find /usr/local/mricron -name 'dcm2niix' -exec rm {} \;
# Delete batch files since it is not available anymore
find /usr/local/mricron -name '*.bat' -exec rm {} \;
find /usr/local/mricron -type d -exec chmod 755 {} \;
find /usr/local/mricron/Resources -type f -exec chmod 644 {} \;
chmod 755 /usr/local/mricron/Resources/pigz_mricron
grep mricron /etc/skel/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> /etc/skel/.bash_aliases
    echo '#MRIcron' >> /etc/skel/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/mricron' >> /etc/skel/.bash_aliases
fi


# Surf-Ice
cd "$HOME"/Downloads
curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/surfice_linux.zip
cd /usr/local
unzip ~/Downloads/surfice_linux.zip
cd Surf_Ice
find . -type d -exec chmod 755 {} \;
find . -type f -exec chmod 644 {} \;
chmod 755 surfice*
chmod 644 surfice_Linux_Installation.txt
grep Surf_Ice /etc/skel/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> /etc/skel/.bash_aliases
    echo '#Surf_Ice' >> /etc/skel/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/Surf_Ice' >> /etc/skel/.bash_aliases
fi


#HCP workbench
#cd "$HOME"/Downloads
#curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/workbench-linux64-v1.4.2.zip
#cd /usr/local
#unzip ~/Downloads/workbench-linux64-v1.4.2.zip
#find /usr/local/workbench -type d -exec chmod 755 {} \;
#grep HCP  /etc/skel/.bash_aliases > /dev/null
#if [ $? -eq 1 ]; then
#    echo '' >> /etc/skel/.bash_aliases
#    echo '#HCP workbench' >> /etc/skel/.bash_aliases
#    echo 'export PATH=$PATH:/usr/local/workbench/bin_linux64' >> /etc/skel/.bash_aliases
#fi


# Tutorial
echo "Install tutorial by Chris Rorden"
cd $HOME/Downloads
if [ ! -e 'tutorial.zip' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/tutorial.zip
fi
cd /etc/skel
unzip ~/Downloads/tutorial.zip
rm -rf /etc/skel/__MACOSX


# Remove MacOS hidden files
find $HOME -name '__MACOSX' -exec rm -rf {} \;
find $HOME -name '.DS_Store' -exec rm -rf {} \;


# Symbolic link to the installer
cat << EOS >> /etc/skel/.profile

# symbolic links
if [ ! -L ~/Desktop/installer ]; then
   ln -fs ~/git/lin4neuro-bionic/installer ~/Desktop
fi

EOS


# Change symbolic link of /bin/sh from dash to bash
echo "dash dash/sh boolean false" | debconf-set-selections
dpkg-reconfigure --frontend=noninteractive dash


##### CLEANUP THE CHROOT ENVIRONMENT #####

# Remove unnecessary configuration files
if [ $(dpkg -l | egrep ^rc | wc -l) -gt 0 ]; then
  dpkg -l | awk '/^rc/ {print $2}' | xargs dpkg --purge
fi


# Clear /var/logs
find /var/log/ -type f -exec cp -f /dev/null {} \;


# Remove $HOME/Downloads
rm -rf $HOME/Downloads


truncate -s 0 /etc/machine-id
rm /sbin/initctl
dpkg-divert --rename --remove /sbin/initctl
apt-get clean
rm -rf /tmp/* ~/.bash_history
umount /proc
umount /sys
umount /dev/pts
export HISTSIZE=0

exit


