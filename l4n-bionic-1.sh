#!/bin/bash

# Lin4Neuro VM building script for Ubuntu 18.04 (Bionic)
# Part1: Setup Lin4Neuro
# This script installs minimal Ubuntu with XFCE 4.12
# and Lin4Neuro theme.
# Pre-requisite: You need to install Ubuntu mini.iso and git beforehand.
# 08-Feb-2021 K. Nemoto

# ChangeLog
# 08-Feb-2021: Refresh scripts from the previous ones

#(optional) Force IPv4
#Uncomment the following two lines if you want the system to use only IPv4
#echo 'Acquire::ForceIPv4 "true";' | sudo tee /etc/apt/apt.conf.d/99force-ipv4
#echo '--inet4-only=1' >> ~/.wgetrc

##### STEP 0. PREPARATION ######################################################

# Update system first
LANG=C
sudo apt-get update; sudo apt-get -y upgrade


# Log
log=$(date +%Y%m%d%H%M%S)-vm1.log
exec &> >(tee -a "$log")


# Set language environments
echo "Begin making Lin4Neuro."

echo "Which language do you want to build? (English/Japanese)"
select lang in "English" "Japanese" "quit"
do
  if [ "$REPLY" = "q" ] ; then
     echo "quit."
     exit 0
  fi
  if [ -z "$lang" ] ; then
     continue
  elif [ $lang == "Japanese" ] ; then

     #Setup Japanese locale
     sudo apt-get install -y language-pack-ja manpages-ja
     sudo update-locale LANG=ja_JP.UTF-8

     #Setup tzdata
     #sudo dpkg-reconfigure tzdata

     #replace us with ja in  /etc/apt/sources.list
     sudo sed -i 's|http://us.|http://ja.|g' /etc/apt/sources.list
     sudo apt-get update

     #Setup Neurodebian repository using repo in Japan
     wget -O- http://neuro.debian.net/lists/bionic.jp.full | \
     sudo tee /etc/apt/sources.list.d/neurodebian.sources.list

     break
  elif [ $lang == "English" ] ; then

     #Setup tzdata
     #sudo dpkg-reconfigure tzdata

     #Setup Neurodebian repository using in repo in us-nh
     wget -O- http://neuro.debian.net/lists/bionic.us-nh.full | \
     sudo tee /etc/apt/sources.list.d/neurodebian.sources.list

     break
  elif [ $lang == "quit" ] ; then
     echo "quit."
     exit 0
  fi
done

# Signature for Neurodebian
#sudo apt-key add ./neuro.debian.net.asc
#Alternative way 1
sudo apt-key adv --recv-keys --keyserver hkps://keyserver.ubuntu.com 0xA5D32F012649A5A9

# Uncomment the following line to install linux-{image,headers}-generic-hwe-18.04
#sudo apt-get -y install linux-{image,headers}-generic-hwe-18.04


##### STEP 1. Install XFCE and utilities #######################################

# Install XFCE
echo "Install XFCE 4.12"
sudo apt-get -y install xfce4 xfce4-terminal xfce4-indicator-plugin \
    xfce4-power-manager-plugins lightdm lightdm-gtk-greeter         \
    shimmer-themes network-manager-gnome xinit build-essential      \
    dkms thunar-archive-plugin file-roller gawk fonts-noto          \
    xdg-utils 


# Install python-related packages
sudo apt-get -y install pkg-config libopenblas-dev liblapack-dev    \
    libhdf5-serial-dev graphviz 
sudo apt-get -y install python3-venv python3-pip python3-dev        \
     python3-tk jupyter-notebook
sudo pip3 install bash_kernel
sudo python3 -m bash_kernel.install
sudo apt-get -y install python-numpy 
 # numpy for python2 is needed for FSL aff2reg


# Install utilities
echo "Install utilities"

sudo apt-get -y install at-spi2-core bc byobu curl wget dc          \
    default-jre evince exfat-fuse exfat-utils gedit                 \
    gnome-system-monitor gnome-system-tools gparted                 \
    imagemagick rename ntp system-config-printer-gnome              \
    system-config-samba tree unzip update-manager vim               \
    wajig xfce4-screenshooter zip ntp tcsh baobab xterm             \
    bleachbit libopenblas-base cups apturl dmz-cursor-theme         \
    chntpw gddrescue p7zip-full gnupg eog meld                      \
    software-properties-common


# Workaround for system-config-samba and networking devices
sudo touch /etc/libuser.conf
sudo touch /etc/NetworkManager/conf.d/10-globally-managed-devices.conf


# Vim settings
cp /usr/share/vim/vimrc ~/.vimrc
sed -i -e 's/"set background=dark/set background=dark/' ~/.vimrc


# Install firefox and libreoffice with language locale
if [ $lang == "English" ] ; then
  #English-dependent packages
  echo "Installation of firefox"
  sudo apt-get -y install firefox firefox-locale-en
  echo "Installation of libreoffice"
  sudo apt-get -y install libreoffice libreoffice-help-en-us
elif [ $lang == "Japanese" ] ; then
  #Japanese-dependent environment
  echo "Installation of firefox and Japanese-related packages"
  sudo apt-get -y install fcitx fcitx-mozc fcitx-config-gtk     \
              unar nkf firefox firefox-locale-ja im-config
  echo "Installation of libreoffice"
  sudo apt-get -y install libreoffice libreoffice-l10n-ja \
    libreoffice-help-ja
  # Change name of directories to English
  LANG=C xdg-user-dirs-update --force
  im-config -n fcitx
fi


# Remove xscreensaver
sudo apt-get -y purge xscreensaver


# (optional) latest Libreoffice
#Uncomment the following three lines to use the latest libreoffice
#sudo add-apt-repository -y ppa:libreoffice/ppa
#sudo apt-get update
#sudo apt-get -y dist-upgrade


##### STEP 2. INSTALL LIN4NEURO ENVIRONMENTS ###################################

# Path of the lin4neuro parts
base_path=$PWD/lin4neuro-parts


# Plymouth-related files
sudo apt-get -y install plymouth-themes plymouth-label


# Lin4neuro-logo
sudo cp -r "${base_path}"/lin4neuro-logo /usr/share/plymouth/themes
sudo update-alternatives --install                                    \
    /usr/share/plymouth/themes/default.plymouth                       \
    default.plymouth                                                  \
    /usr/share/plymouth/themes/lin4neuro-logo/lin4neuro-logo.plymouth \
    100
sudo update-initramfs -u -k all


# Icons
mkdir -p ~/.local/share
cp -r "${base_path}"/local/share/icons ~/.local/share/


# Customized menu
mkdir -p ~/.config/menus
cp "${base_path}"/config/menus/xfce-applications.menu ~/.config/menus

# Desktop files
cp -r "${base_path}"/local/share/applications ~/.local/share/


# Neuroimaging.directory
mkdir -p ~/.local/share/desktop-directories
cp "${base_path}"/local/share/desktop-directories/Neuroimaging.directory ~/.local/share/desktop-directories


# Background image (and remove unnecessary image file)
sudo cp "${base_path}"/backgrounds/deep_ocean.png /usr/share/backgrounds
sudo rm /usr/share/backgrounds/xfce/xfce-teal.jpg


# Modified lightdm-gtk-greeter.conf
sudo mkdir -p /usr/share/lightdm/lightdm-gtk-greeter.conf.d
sudo cp "${base_path}"/lightdm/lightdm-gtk-greeter.conf.d/01_ubuntu.conf /usr/share/lightdm/lightdm-gtk-greeter.conf.d


# Auto-login
sudo mkdir -p /usr/share/lightdm/lightdm.conf.d
sudo cp "${base_path}"/lightdm/lightdm.conf.d/10-ubuntu.conf /usr/share/lightdm/lightdm.conf.d


# Cusotmized panel, dsktop, and theme
cp -r "${base_path}"/config/xfce4 ~/.config


# Clean packages
sudo apt-get -y autoremove


# GRUB customization to show GRUB on boot
sudo sed -i -e 's/GRUB_TIMEOUT_STYLE/#GRUB_TIMEOUT_STYLE/' /etc/default/grub
sudo sed -i -e 's/GRUB_TIMEOUT=0/GRUB_TIMEOUT=10/' /etc/default/grub
sudo update-grub


# Boot repair
sudo add-apt-repository -y ppa:yannubuntu/boot-repair
sudo apt-get install -y boot-repair


# alias
cat << EOS >> ~/.bash_aliases

#alias for xdg-open
alias open='xdg-open &> /dev/null'

EOS


echo "Finished!"
echo "Please run l4n-bionic-2.sh to install neuroimaging packages after reboot."

exit

