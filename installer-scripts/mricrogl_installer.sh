#!/bin/bash

#MRIcroGL
echo "Install MRIcroGL"
cd $HOME/Downloads

curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/MRIcroGL_linux1804.zip

cd /usr/local
sudo unzip ~/Downloads/MRIcroGL_linux1804.zip

grep MRIcroGL ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> ~/.bash_aliases
    echo '# MRIcroGL' >> ~/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/MRIcroGL' >> ~/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/MRIcroGL/Resources' >> ~/.bash_aliases
fi

