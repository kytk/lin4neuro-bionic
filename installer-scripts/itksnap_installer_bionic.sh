#!/bin/bash

echo "Install ITK-SNAP"
cd $HOME/Downloads

if [ ! -e 'itksnap-3.8.0-20190612-Linux-x86_64.tar.gz' ]; then
  curl -O  http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/itksnap-3.8.0-20190612-Linux-x86_64.tar.gz
fi

cd /usr/local
sudo tar xvzf ~/Downloads/itksnap-3.8.0-20190612-Linux-x86_64.tar.gz
sudo mv itksnap-3.8.0-20190612-Linux-gcc64 itksnap
grep itksnap ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> ~/.bash_aliases
    echo '#ITK-SNAP' >> ~/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/itksnap/bin' >> ~/.bash_aliases
fi

#make icon show in the neuroimaging directory
sed -i 's/NoDisplay=true/NoDisplay=false/' ~/.local/share/applications/itksnap.desktop


#c3d
echo "Install c3d"
[ ! -d $HOME/bin ] && mkdir $HOME/bin
cp -r ~/git/lin4neuro-bionic/lin4neuro-parts/bin/bashcomp.sh $HOME/bin

cd "$HOME"/Downloads

if [ ! -e 'c3d-1.0.0-Linux-x86_64.tar.gz' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/c3d-1.0.0-Linux-x86_64.tar.gz
fi

cd /usr/local
sudo tar xvzf ~/Downloads/c3d-1.0.0-Linux-x86_64.tar.gz
sudo mv c3d-1.0.0-Linux-x86_64 c3d

grep c3d ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> ~/.bash_aliases
    echo '#c3d' >> ~/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/c3d/bin' >> ~/.bash_aliases
    echo 'source $HOME/bin/bashcomp.sh' >> ~/.bash_aliases
fi


echo "Finished!"
sleep 5
exit
