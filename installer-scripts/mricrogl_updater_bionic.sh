#!/bin/bash
# Update script for MRIcroGL
# K. Nemoto 30 Aug 2022

#Edit the latest version#
latest=v1.2.20220720
#########################

# cd to the directory containing the script
cd "${0%/*}"

# Check the version currently installed
ver=$(dcm2niix -v | sed -n 2p)

echo "Your version is ${ver}"

if [ $latest != $ver ];then
  echo "Latest MRIcroGL available."
  while true
  do
    echo "Do you want to update MRIcroGL including dcm2niix (yes/no)? "
  
    read answer
  
    case $answer in
      [Yy]*)
        break
        ;;
      [Nn]*)
        echo -e "Abort. \n"
        exit
        ;;
      *)
        echo -e "Type yes or no.\n"
        ;;
    esac
  done
  echo "Update MRIcroGL"
  cd "$HOME"/Downloads
  [ -e MRIcroGL_linux1804.zip ] && rm MRIcroGL_linux1804.zip
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/MRIcroGL_linux1804.zip
  cd /usr/local
  sudo rm -rf MRIcroGL
  sudo unzip ~/Downloads/MRIcroGL_linux1804.zip
  echo "Update complete"
else
  echo "Your version of dcm2niix is up-to-date."
fi

exit

