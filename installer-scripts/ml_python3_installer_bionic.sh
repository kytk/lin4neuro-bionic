#!/bin/bash
#A script to install python libraries for machine learning and Spyder3

echo "This script installs various Python3 libraries for machine learning."
echo "Do you want to continue? (yes/no)"

read answer

case $answer in
        [Yy]*)
		echo -e "Install libraries and Spyder3 \n"
		#Installation of python libraries for machine learning
		sudo apt-get install -y jupyter-notebook python3-numpy \
			python3-scipy python3-opencv python3-dateutil
		pip3 install --user cmake matplotlib pyyaml h5py pydot-ng \
			keras pillow pandas
		pip3 install --user tensorflow
		
		#Installation of Spyder3
		pip3 install --user PyQtWebEngine spyder

                break
                ;;
        [Nn]*)
                echo -e "Run this script later.\n"
                exit
                ;;
        *)
                echo -e "Type yes or no.\n"     
                ;;
esac


