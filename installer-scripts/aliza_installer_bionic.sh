#!/bin/bash

echo "Install Aliza"
cd $HOME/Downloads

if [ ! -e 'aliza_1.98.47.1.deb' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/aliza_1.98.47.1.deb
fi

sudo apt install -y ./aliza_1.98.47.1.deb

#make icon show in the neuroimaging directory
sed -i 's/NoDisplay=true/NoDisplay=false/' ~/.local/share/applications/aliza.desktop

echo "Finished!"
sleep 5
exit
