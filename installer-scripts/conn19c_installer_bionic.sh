#!/bin/bash
#CONN19c standalone installer

#Check MCR is installed
if [ ! -d /usr/local/MATLAB/MCR/v95 ]; then
  echo "Matlab Compiler Runtime needs to be installed first!"
  ~/git/lin4neuro-bionic/installer-scripts/mcr_v95_installer_bionic.sh
fi

#Download CONN18b standalone
echo "Download CONN19c standalone"
cd $HOME/Downloads

if [ ! -e 'conn19c_glnxa64.zip' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/conn19c_glnxa64.zip
fi

cd /usr/local
sudo mkdir conn19c_standalone
cd conn19c_standalone
sudo unzip ~/Downloads/conn19c_glnxa64.zip

sed -i 's/NoDisplay=true/NoDisplay=false/' ~/.local/share/applications/conn19c.desktop

#alias
echo '' >> ~/.bash_aliases
echo '#conn19c standalone' >> ~/.bash_aliases
echo "alias conn='/usr/local/conn19c_standalone/run_conn.sh /usr/local/MATLAB/MCR/v95'" >> ~/.bash_aliases


echo "Finished! Run CONN from menu -> Neuroimaging -> CONN"
sleep 5 
exit

