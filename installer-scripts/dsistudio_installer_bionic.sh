#!/bin/bash
# Installer script for DSI studio

#echo "Install Qt 5.12.8"
#sudo add-apt-repository -y ppa:beineri/opt-qt-5.12.8-bionic
#sudo apt-get update
#sudo apt install -y qt512base qt512charts-no-lgpl

echo "Install DSI Studio"
cd $HOME/Downloads

if [ ! -e 'dsi_studio_ubuntu_1804.zip' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/dsi_studio_ubuntu_1804.zip
fi

cd /usr/local
sudo unzip ~/Downloads/dsi_studio_ubuntu_1804.zip
sudo chmod 755 dsi-studio

#make icon show in the neuroimaging directory
sed -i 's/NoDisplay=true/NoDisplay=false/' ~/.local/share/applications/dsistudio.desktop

echo "Finished!"
sleep 5
exit

